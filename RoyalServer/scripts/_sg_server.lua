local function ToggleOffPhysics(inst)
    inst.sg.statemem.isphysicstoggle = true
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.GROUND)
end

local function ToggleOnPhysics(inst)
    inst.sg.statemem.isphysicstoggle = nil
    inst.Physics:ClearCollisionMask()
    inst.Physics:CollidesWith(COLLISION.WORLD)
    inst.Physics:CollidesWith(COLLISION.OBSTACLES)
    inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
    inst.Physics:CollidesWith(COLLISION.CHARACTERS)
    inst.Physics:CollidesWith(COLLISION.GIANTS)
end

local function ClearStatusAilments(inst)
    if inst.components.freezable ~= nil and inst.components.freezable:IsFrozen() then
        inst.components.freezable:Unfreeze()
    end
    if inst.components.pinnable ~= nil and inst.components.pinnable:IsStuck() then
        inst.components.pinnable:Unstick()
    end
end

local function ForceStopHeavyLifting(inst)
    if inst.components.inventory:IsHeavyLifting() then
        inst.components.inventory:DropItem(
            inst.components.inventory:Unequip(EQUIPSLOTS.BODY),
            true,
            true
        )
    end
end

local STATES_TO_ADD = {
	--[[
	State{
        name = "jump_and_fly",
        tags = { "doing", "busy", "canrotate", "notalking", "pausepredict" },

        onenter = function(inst)
			if inst.DynamicShadow then
				inst.DynamicShadow:Enable(false)
			end
			
			if inst.components.playercontroller ~= nil then
				inst.components.playercontroller:RemotePausePrediction()
				inst.components.playercontroller:Enable(false)
			end
			
            inst.components.locomotor:Stop()
			
            inst.AnimState:PlayAnimation("jump_pre")
			inst.AnimState:PushAnimation("jump")
        end,

		timeline =
        {
            TimeEvent(267 * FRAME, function(inst)
				if inst.components.playercontroller ~= nil then
					inst.components.playercontroller:Enable(true)
				end
				
                inst.sg:GoToState("idle")
            end),
		},
    },]]
	
	State{
        name = "fly_idle",
        tags = { "idle", "canrotate", "notalking"},

        onenter = function(inst, pushanim)
			print "fly_idle"
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
			
			ToggleOffPhysics(inst)
			
			if inst.DynamicShadow then
				inst.DynamicShadow:Enable(false)
			end
			
			inst.AnimState:PlayAnimation("flying_loop", true)
			
			inst.Physics:SetMotorVel(0, TUNING.PLAYER_FALL_SPPED, 0)
        end,
		
		onexit = function(inst)
			inst.Physics:Stop()
		end,
    },
	--[[
	State{
        name = "fly_start",
        tags = { "moving", "running", "canrotate", "autopredict", "notalking" },

        onenter = function(inst, no_pre)
			print "fly_start"
			ToggleOffPhysics(inst)
			
            inst.components.locomotor:RunForward()
			
			inst.AnimState:PlayAnimation("flying_loop")
			
            inst.sg.mem.footsteps = 0
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,

		events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("fly")
                end
            end),
        },
    },]]
	
	State{
        name = "fly",
        tags = { "moving", "running", "canrotate", "autopredict", "notalking" },

        onenter = function(inst) 
			print("fly")
			ToggleOffPhysics(inst)
			
			if inst.DynamicShadow then
				inst.DynamicShadow:Enable(false)
			end
			
            inst.components.locomotor:RunForward()

			inst.AnimState:PlayAnimation("flying_loop", true)
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("fly")
        end,
    },

    State{
        name = "fly_stop",
        tags = { "canrotate", "idle", "autopredict", "notalking" },

        onenter = function(inst, stop)
			print("fly_stop")
            inst.components.locomotor:Stop()
			
			if stop then
				inst.AnimState:PlayAnimation("flying_land")
				
				if inst.DynamicShadow then
					inst.DynamicShadow:Enable(true)
				end
				
				if inst.components.playercontroller ~= nil then
					inst.components.playercontroller:RemotePausePrediction()
					inst.components.playercontroller:Enable(false)
				end
				
				ToggleOnPhysics(inst)
			else
				inst.Physics:SetMotorVel(0, TUNING.PLAYER_FALL_SPPED, 0)
			end
        end,
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
					
					if inst.components.playercontroller ~= nil then
						inst.components.playercontroller:Enable(true)
					end
                end
            end),
        },
		
		onexit = function(inst)
			inst.Physics:Stop()
		end,
    },
	
	State{
        name = "sinking_death",
        tags = { "busy", "pausepredict", "nomorph" },

        onenter = function(inst)
            assert(inst.deathcause ~= nil, "Entered sinking_death state without cause.")

            ClearStatusAilments(inst)
            ForceStopHeavyLifting(inst)

            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
			inst.Physics:Stop()
			
            inst:ClearBufferedAction()

			inst.SoundEmitter:PlaySound("dontstarve/wilson/death")

			if not inst:HasTag("mime") then
				inst.SoundEmitter:PlaySound((inst.talker_path_override or "dontstarve/characters/")..(inst.soundsname or inst.prefab).."/death_voice")
			end

			inst.AnimState:Hide("swap_arm_carry")
			inst.AnimState:PlayAnimation("boat_death")
			
			SpawnAt("boat_death", inst)
          
            inst.components.burnable:Extinguish()

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

            --Don't process other queued events if we died this frame
            inst.sg:ClearBufferedEvents()
        end,

        onexit = function(inst)
            --You should never leave this state once you enter it!
			assert(false, "Left sinking_death state.")
        end,
		
		timeline =
        {
            TimeEvent(50*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/boat_sinking_shadow")
            end),
            TimeEvent(70*FRAMES, function(inst)
                inst.DynamicShadow:Enable(false)
            end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
                end
            end),
        },
    },
}
--Добавляем
for i, state in ipairs(STATES_TO_ADD) do
	AddStategraphState("wilson", state)
end

--Перехватываем ивенты, отвечающие за передвижение, и вставляем своё
AddStategraphPostInit("wilson", function(inst)
    local _locomotefn = inst.events.locomote.fn
    inst.events.locomote.fn = function(inst, data, ...)
		if inst.sg:HasStateTag("busy") or inst:HasTag("busy") then
			return
		end
		
		local is_moving = inst.sg:HasStateTag("moving")
		local should_move = inst.components.locomotor:WantsToMoveForward()

		if inst.components.flight_tracker and inst.components.flight_tracker:IsFlying() then
			if is_moving and not should_move then
				inst.sg:GoToState("fly_stop")
			elseif not is_moving and should_move then
				inst.sg:GoToState("fly")
			end
			
			return
		end

		_locomotefn(inst, data, ...)
	end

	local _deathfn = inst.events.death.fn
	inst.events.death.fn = function(inst, data)
		if data.cause == "drowning" then
			inst.sg:GoToState("death_boat")
			return
		end
		
		_death_eventhandler(inst, data)
	end
	
	local _idleonenter = inst.states.idle.onenter
	inst.states.idle.onenter = function(inst, ...)
		if inst.components.flight_tracker and inst.components.flight_tracker:IsFlying() then
			inst.sg:GoToState("fly_idle")
			return
		end
		
		_idleonenter(inst, ...)
	end
end)
