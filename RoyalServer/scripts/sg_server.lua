local function ClearStatusAilments(inst)
    if inst.components.freezable ~= nil and inst.components.freezable:IsFrozen() then
        inst.components.freezable:Unfreeze()
    end
    if inst.components.pinnable ~= nil and inst.components.pinnable:IsStuck() then
        inst.components.pinnable:Unstick()
    end
end

local function ForceStopHeavyLifting(inst)
    if inst.components.inventory:IsHeavyLifting() then
        inst.components.inventory:DropItem(
            inst.components.inventory:Unequip(EQUIPSLOTS.BODY),
            true,
            true
        )
    end
end

local states_to_add = {
	State{
        name = "sinking_death",
        tags = { "busy", "pausepredict", "nomorph", "notalking" },

        onenter = function(inst)
            assert(inst.deathcause ~= nil, "Entered sinking_death state without cause.")
            ClearStatusAilments(inst)
            ForceStopHeavyLifting(inst)

            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
			inst.Physics:Stop()
			
            inst:ClearBufferedAction()

			inst.SoundEmitter:PlaySound("dontstarve/wilson/death")

			if not inst:HasTag("mime") then
				inst.SoundEmitter:PlaySound((inst.talker_path_override or "dontstarve/characters/")..(inst.soundsname or inst.prefab).."/death_voice")
			end

			inst.AnimState:Hide("swap_arm_carry")
			inst.AnimState:PlayAnimation("boat_death")
			
			SpawnAt("boat_death", inst)
          
            inst.components.burnable:Extinguish()

            if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:RemotePausePrediction()
                inst.components.playercontroller:Enable(false)
            end

            --Don't process other queued events if we died this frame
            inst.sg:ClearBufferedEvents()
        end,

        onexit = function(inst)
            --You should never leave this state once you enter it!
			assert(false, "Left sinking_death state.")
        end,
		
		timeline =
        {
            TimeEvent(50*FRAMES, function(inst)
                inst.SoundEmitter:PlaySound("dontstarve_DLC002/common/boat_sinking_shadow")
            end),
            TimeEvent(70*FRAMES, function(inst)
                inst.DynamicShadow:Enable(false)
            end),
        },
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
                end
            end),
        },
    },
	
	State{
        name = "fly_idle",
        tags = { "idle", "canrotate", "notalking" },

        onenter = function(inst, pushanim)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
			inst.Physics:Stop()

            if not inst.AnimState:IsCurrentAnimation("flying_loop") or inst.AnimState:AnimDone() then
				if pushanim then
					inst.AnimState:PlayAnimation("flying_loop", true)
				else
					inst.AnimState:PushAnimation("flying_loop", true)
				end
            end
        end,
    },
	
	State{
        name = "fly_start",
        tags = { "moving", "running", "canrotate", "autopredict", "notalking" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("flying_loop")
            inst.sg.mem.footsteps = 0
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("fly")
                end
            end),
        },
    },

    State{
        name = "fly",
        tags = { "moving", "running", "canrotate", "autopredict", "notalking" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()

            if not inst.AnimState:IsCurrentAnimation("flying_loop") or inst.AnimState:AnimDone() then
                inst.AnimState:PlayAnimation("flying_loop", true)
            end

            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("fly")
        end,
    },

    State{
        name = "fly_stop",
        tags = { "canrotate", "idle", "autopredict", "notalking" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation("flying_loop")
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
}

for _, state in ipairs(states_to_add) do
	AddStategraphState("wilson", state)
end

AddStategraphPostInit("wilson", function(self)
	self.events.locomote.fn = function(inst, data)
		if inst.sg:HasStateTag("busy") then
            return
        end
		
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		local is_flying = inst.components.flight_tracker and inst.components.flight_tracker:IsFlying()

        if inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent") or inst.sg:HasStateTag("waking") then -- wakeup on locomote
            if inst.sleepingbag ~= nil and inst.sg:HasStateTag("sleeping") then
                inst.sleepingbag.components.sleepingbag:DoWakeUp()
                inst.sleepingbag = nil
            end
        elseif is_moving and not should_move then
            inst.sg:GoToState((is_flying and "fly" or "run").."_stop")
        elseif not is_moving and should_move then
            inst.sg:GoToState((is_flying and "fly" or "run").."_start")
        elseif data.force_idle_state and not (is_moving or should_move or inst.sg:HasStateTag("idle")) then
            inst.sg:GoToState("idle")
        end
	end
	
	local _idle = self.states.idle.onenter
	self.states.idle.onenter = function(inst, pushanim)
		if inst.components.flight_tracker and inst.components.flight_tracker:IsFlying() then
			inst.sg:GoToState("fly_idle", pushanim)
		else
			_idle(inst, pushanim)
		end
	end
end)
