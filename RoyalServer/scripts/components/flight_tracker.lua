local function ToggleOffPhysics(inst)	
	if not inst.phys_disabled then
		inst.phys_disabled = true
		
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.GROUND)
	end
end

local function ToggleOnPhysics(inst)
	if inst.phys_disabled then
		inst.phys_disabled = nil
		
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.WORLD)
		inst.Physics:CollidesWith(COLLISION.OBSTACLES)
		inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
		inst.Physics:CollidesWith(COLLISION.CHARACTERS)
		inst.Physics:CollidesWith(COLLISION.GIANTS)
	end
end

local function OnDistChanged(self, val)
	self.inst.replica.flight_tracker:SetDistToGround(val)
end

local FlightTracker = Class(function(self, inst)
    self.inst = inst
	self.dist_to_ground = 0
	self.enabled = false
end,
nil,
{
	dist_to_ground = OnDistChanged,
})

function FlightTracker:Enable(val)
	if val then
		ToggleOffPhysics(self.inst)
		self.inst:StartUpdatingComponent(self)
	else
		self.inst:StopUpdatingComponent(self)
		self.dist_to_ground = 0
		ToggleOnPhysics(self.inst)
	end
end

function FlightTracker:IsFlying()
	return self.dist_to_ground > 0
end

function FlightTracker:OnUpdate(dt)
	local x,y,z = self.inst.Transform:GetWorldPosition()

	if x and y and z then 
		
		local vely = 0 
		if self.inst.Physics then 
			local vx, vy, vz = self.inst.Physics:GetVelocity()
			vely = vy or 0
		end
		
		local dist_to_ground = y + vely * dt * 1.5

		if dist_to_ground < 0.01 and vely <= 0 then
			if not self.inst:IsOnValidGround() then
				self.inst.components.land_indicator:Enable(false)
				if self.inst.components.health and self.inst.components.health.currenthealth > 0 then
					self.inst.components.health:DoDelta(-self.inst.components.health.currenthealth, nil, "drowning")
				end
			else
				self.inst:StartFlying(false)
			end
			
			self.dist_to_ground = 0
			self.inst:StopUpdatingComponent(self) 
		else
			if dist_to_ground <= 5 then
				ToggleOnPhysics(self.inst)
			end
			--[[
			local is_moving = self.inst.sg:HasStateTag("moving")
			local should_move = self.inst.components.locomotor:WantsToMoveForward()
		
			if not is_moving and not should_move then
				self.inst.Physics:SetMotorVel(0, TUNING.PLAYER_FALL_SPPED, 0)
				print(GetTime(), "Vel set!")
			end]]
			
			self.dist_to_ground = dist_to_ground
		end        
	else
		self.inst:StopUpdatingComponent(self) 
	end 
end

return FlightTracker