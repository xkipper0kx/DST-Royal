local function OnWorkingChanged(self, val)
	self.inst.replica.land_indicator:Enable(val)
end

local LandIndicator = Class(function(self, inst)
    self.inst = inst
	self.working = false
end,
nil,
{
	working = OnWorkingChanged,
})

function LandIndicator:Enable(val)
	self.working = val
end

return LandIndicator