modimport("scripts/libs/lib_ver.lua")

if not SERVER_SIDE then 
	return
end

if mods.royal_server ~= nil then
	print("ROYAL SERVER ERROR! Mod already enabled!")
	return
end

mods.royal_server = {
	root = MODROOT,
	version = modinfo.version
}

_G.CHEATS_ENABLED = true
require "debugkeys"
require "keys_for_debuging"

AddPlayersPostInit(function(inst)
	inst.AnimState:AddOverrideBuild("player_boat_death")
	
	local components = {
		"flight_tracker",
		"land_indicator",
	}
	
	for i, comp in ipairs(components) do
		if not inst.components[comp] then
			inst:AddComponent(comp)
		end
	end
	
	inst.Physics:SetDamping(1)--Не падаем
	
	function inst:StartFlying(togle) --ThePlayer:StartFlying(true)
		if togle then
			local x, y, z = inst.Transform:GetWorldPosition()
			inst.Physics:Teleport(x, 40, z)
			
			inst:AddTag("flying")
			--inst.sg:GoToState("idle") --!!!
			
			inst.components.flight_tracker:Enable(true)
			inst.components.land_indicator:Enable(true)
		else
			local x, y, z = inst.Transform:GetWorldPosition()
			inst.components.land_indicator:Enable(false)
			
			inst:RemoveTag("flying")
			--inst.sg:GoToState("fly_stop", true)
		end
	end
	
	--Заменяем ивент смерти
	local _ondeath = inst.sg.sg.events.death.fn
	inst.sg.sg.events.death.fn = function(inst, data)
		if not inst:IsOnValidGround() then
			inst.sg:GoToState("sinking_death")
		else
			_ondeath(inst, data)
		end
	end
	
	--Т.к в локомоторе нужно было бы заменять большие части кода, просто заменяем всю физику 
	--[[
	local Physics = getmetatable(inst.Physics).__index
	ReplaceFn(Physics, "SetMotorVel", function(old, self, x, y, z)
		if inst.components.flight_tracker and inst.components.flight_tracker:IsFlying() then
			old(self, x, TUNING.PLAYER_FALL_SPPED, z)
		else
			old(self, x, y, z)
		end
	end)]]
end)

modimport("scripts/sg_server.lua")
