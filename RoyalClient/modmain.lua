modimport("scripts/libs/lib_ver.lua")

if mods.roayal_client ~= nil then
	print("ROYAL CLIENT ERROR! Mod already enabled!")
	return
end

mods.roayal_client = {
	root = MODROOT,
	version = modinfo.version
}

local TheServerData = require "server_data"
local ToLoad = require "royal_to_load"
local Networking = require "royal_networking"
local LocomotorPatches = require "locomotor_patches"
local UI = require "royal_ui"

TUNING.PLAYER_FALL_SPPED = -2.5

--Подгружаем нашу модную систему файлов
GetGlobal("TheServerData", TheServerData)

Assets = ToLoad.Assets
PrefabFiles = ToLoad.Prefabs

for pref, fn in pairs(Networking) do
	AddPrefabPostInit(pref, fn)
end

for class, fn in pairs(UI) do
	AddClassPostConstruct(class, fn)
end

modimport "scripts/sg_client.lua"

for _, lng in ipairs({"main", "rus"}) do
	modimport("scripts/strings/"..lng..".lua")
end

local REPLICAS = {
	"land_indicator",
	"flight_tracker",
}

for i, comp in ipairs(REPLICAS) do
	AddReplicableComponent(comp)
end

AddNetworkPostInit(function(w)
	w:AddComponent("worldcharacterselectlobby")
end)

--Так как этот компонент есть на обоих сторонах,
--нам нужно и фиксить его тут
AddComponentPostInit("locomotor", function(self)--[[
	for name, fn in pairs(LocomotorPatches) do
		self[name] = fn
	end]]
end)