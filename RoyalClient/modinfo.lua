name						= " RoyalClient"
author						= "Cunning fox"
version						= "1.0"

description					= ""
forumthread					= ""

all_clients_require_mod 	= true
client_only_mod 			= false

dst_compatible				= true

icon_atlas					= "images/preview.xml"
icon						= "preview.tex"

api_version					= 10
--priority					= -1.0414582*1000*1000 - 1
priority = 0

game_modes = {
	{
		name = "royal",
		label = "DST Royal",
		settings = {
			level_type = "SURVIVAL",
			spawn_mode = "fixed",
			resource_renewal = false,
			ghost_sanity_drain = false,
			ghost_enabled = false,
			revivable_corpse = false, 
			spectator_corpse = false,
			portal_rez = false,
			reset_time = nil,
			invalid_recipes = nil,
			--
			max_players = 25,
			--override_item_slots = 0,
            drop_everything_on_despawn = true,
			no_air_attack = true,
			no_crafting = false,
			no_minimap = false,
			no_hunger = false,
			no_sanity = false,
			no_avatar_popup = false,
			no_morgue_record = true,
			override_normal_mix = nil,
			override_lobby_music = "dontstarve/music/lava_arena/FE2",
			cloudcolour = { 0, 0, 0 },
			lobbywaitforallplayers = true,
			hide_worldgen_loading_screen = true,
			hide_received_gifts = true,
			skin_tag = nil,
			
			countdown_time = 610,
			no_new_connections = 20,
			no_character_select = 5,
			no_character_select = 5,
		},
	}
}