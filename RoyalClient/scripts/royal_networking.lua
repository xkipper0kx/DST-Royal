local function OnCameraDirty(inst)
	local self = TheCamera
	if not self then return end
	
	local data = json.decode(inst.net_camera:value())
	if data then
		if data.default then
			self:SetDefault()
		else
			for name, val in pairs(data) do
				self[name] = val
			end
		end
	end
end

return {
	["player_classified"] = function(inst)
		inst.net_camera = net_string(inst.GUID, "dst_royal.camera", "net_camera_dirty") --ThePlayer.player_classified.net_camera:set(json.encode({mindistpitch = 90, distancetarget = 10}))
		
		if not TheNet:IsDedicated() then
			local event_data = {
				net_camera_dirty = OnCameraDirty,
			}
			
			for event, fn in pairs(event_data) do
				inst:ListenForEvent(event, fn)
			end
		end
	end,
}
