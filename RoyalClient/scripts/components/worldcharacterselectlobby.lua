--[[
	Adapted by CunningFox.
	Так как этото компонент был сделан только длыя ивентов, нам нужно его заменить,
	И вырезать всё лишнее.
	Компонент полностью переписан и переделан так, чтоб игроки не голосовали за старт,
	а сразу начинался обратный отсчет. Сохранены только функциbи, соответственно
]]

--------------------------------------------------------------------------
--[[ WorldCharacterSelectLobby class definition ]]
--------------------------------------------------------------------------

return Class(function(self, inst)

--------------------------------------------------------------------------
--[[ Constants ]]
--------------------------------------------------------------------------
local COUNTDOWN_TIME = GetGameModeProperty("countdown_time")
local COUNTDOWN_NOCONNECTIONS = GetGameModeProperty("no_new_connections")
local COUNTDOWN_NOCHARACTERSELECT = GetGameModeProperty("no_character_select")
local COUNTDOWN_INACTIVE = 65535
--------------------------------------------------------------------------
--[[ Member variables ]]
--------------------------------------------------------------------------

--Public
self.inst = inst

--Private
local _world = TheWorld
local _ismastersim = _world.ismastersim

local _lobby_up_time = 0
local _client_wait_time = {}

--Master simulation
local _countdownf = -1

--Network
local _countdowni = net_ushortint(inst.GUID, "worldcharacterselectlobby._countdowni", "spawncharacterdelaydirty")
local _lockedforshutdown = net_bool(inst.GUID, "worldcharacterselectlobby._lockedforshutdown", "lockedforshutdown")

--------------------------------------------------------------------------
--[[ Local Functions ]]
--------------------------------------------------------------------------
local function GetPlayersClientTable()
    local clients = TheNet:GetClientTable() or {}
    if not TheNet:GetServerIsClientHosted() then
		for i, v in ipairs(clients) do
			if v.performance ~= nil then
				table.remove(clients, i) -- remove "host" object
				break
			end
		end
    end
    return clients
end
--------------------------------------------------------------------------
--[[ Global Setup ]]
--------------------------------------------------------------------------

AddUserCommand("playerreadytostart", {
    prettyname = nil, --default to STRINGS.UI.BUILTINCOMMANDS.RESCUE.PRETTYNAME
    desc = nil, --default to STRINGS.UI.BUILTINCOMMANDS.RESCUE.DESC
    permission = COMMAND_PERMISSION.USER,
    slash = false,
    usermenu = false,
    servermenu = false,
    params = {"ready"},
    vote = false,
    canstartfn = function(command, caller, targetid)
        return false
    end,
    serverfn = function(params, caller)
		--TogglePlayerReadyToStart(caller.userid)
    end,
})

--------------------------------------------------------------------------
--[[ Private Server event handlers ]]
--------------------------------------------------------------------------
local function CalcLobbyUpTime()
	return _lobby_up_time and (GetTimeRealSeconds() - _lobby_up_time) or 0
end

local function CloseLobby(val)
	TheNet:SetAllowNewPlayersToConnect(not val)
    TheNet:SetIsMatchStarting(val)
end

local function StarTimer(time)
	print ("[WorldCharacterSelectLobby] Countdown started")

	_countdownf = time
    _countdowni:set(math.ceil(time))

	self.inst:StartWallUpdatingComponent(self)
end

local function TryStartCountdown()
	StarTimer(COUNTDOWN_TIME)
end

local function OnRequestLobbyCharacter(world, data)
	if data == nil or not self:IsAllowingCharacterSelect() then
		return
	end

	local client = TheNet:GetClientTableForUser(data.userid)
	if not client then
		return
	end

	TheNet:SetLobbyCharacter(data.userid, data.prefab_name, data.skin_base, data.clothing_body, data.clothing_hand, data.clothing_legs, data.clothing_feet)
end

--------------------------------------------------------------------------
--[[ Private Client event handlers ]]
--------------------------------------------------------------------------

local function OnCountdownDirty()
	print("OnCountdownDirty", _countdowni:value())
    if _ismastersim then
		if _countdowni:value() == 0 then
			inst:StopWallUpdatingComponent(self)
			print("[WorldCharacterSelectLobby] Countdown finished")

			--Use regular update to poll for when to clear match starting flag
			inst:StartUpdatingComponent(self)
		elseif _countdowni:value() == COUNTDOWN_NOCONNECTIONS then
			CloseLobby(true)
		end
    end

    local t = _countdowni:value()
    _world:PushEvent("lobbyplayerspawndelay", { time = t, active = t ~= COUNTDOWN_INACTIVE })
end

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------

--Initialize network variables
_countdowni:set(COUNTDOWN_INACTIVE)
self.inst:DoTaskInTime(0, TryStartCountdown)
--Register network variable sync events
inst:ListenForEvent("spawncharacterdelaydirty", OnCountdownDirty)

if _ismastersim then
    TheNet:SetIsMatchStarting(false) --Reset flag in case it's invalid

    --Register events
    inst:ListenForEvent("ms_requestedlobbycharacter", OnRequestLobbyCharacter, _world)
end

--------------------------------------------------------------------------
--[[ Public members ]]
--------------------------------------------------------------------------
if _ismastersim then
	function self:IsAllowingCharacterSelect()
		if _countdowni:value() > COUNTDOWN_NOCHARACTERSELECT and not _lockedforshutdown:value() then
			return true
		end
		
		return false
	end 
	
	function self:OnPostInit()
		if TheNet:GetDeferredServerShutdownRequested() then
			_lockedforshutdown:set(true)
		end
	end
end

function self:GetSpawnDelay()
	local delay = _countdowni:value()
	return delay ~= COUNTDOWN_INACTIVE and delay or -1
end

function self:IsServerLockedForShutdown()
	return _lockedforshutdown:value()
end

function self:IsPlayerReadyToStart(userid)
	return false
end

-- TheWorld.net.components.worldcharacterselectlobby:Dump()
function self:Dump()
	local str = ""
	for i, v in ipairs(_players_ready_to_start) do
		str = str .. ", " .. tostring(v:value())
	end
	print(str)
end

--------------------------------------------------------------------------
--[[ Update ]]
--------------------------------------------------------------------------
local i
function self:OnWallUpdate(dt)
	i = i+1
	print("OnWallUpdate: "..dt, i)
	_countdownf = math.max(0, _countdownf - dt)
	local c = math.ceil(_countdownf)
	if _countdowni:value() ~= c then
		_countdowni:set(c)
	end
end

function self:OnUpdate(dt)
    if #AllPlayers <= 0 then
        local isdedicated = not TheNet:GetServerIsClientHosted()
        local index = 1
        for i, v in ipairs(TheNet:GetClientTable()) do
            if not isdedicated or v.performance == nil then
                --Still someone connected
                return
            end
        end
    end
    --Either someone has spawned in, or everybody disconnceted
    TheNet:SetIsMatchStarting(false)
    inst:StopUpdatingComponent(self)
end

--------------------------------------------------------------------------
--[[ Save/Load ]]
--------------------------------------------------------------------------

if _ismastersim then
	function self:OnSave()
		local data =
		{
			match_started = _countdowni:value() == 0,
		}

		return data
	end 
	
	function self:OnLoad(data)
		if data then
			if data.match_started then
				_countdownf = 0
				_countdowni:set(0)
			end
		end
	end
end

end)
