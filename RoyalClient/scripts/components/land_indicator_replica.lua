local LandIndicator = Class(function(self, inst)
    self.inst = inst
	
	self.enabled = net_bool(inst.GUID, "landindicator.enabled", "landindicatorenableddirty")
	
    if not TheWorld.ismastersim then
		inst:ListenForEvent("landindicatorenableddirty", function(inst) 
			local self = inst.replica.land_indicator
			if self.enabled:value() then
				self:CreateIndicator()
				self.inst:StartUpdatingComponent(self)
			else
				self:RemoveIndicator()
				self.inst:StopUpdatingComponent(self)
			end
		end)
	end
end)

function LandIndicator:Enable(val)
	self.enabled:set(val)
end

function LandIndicator:CreateIndicator()
	self:RemoveIndicator()

	self.ind = SpawnPrefab("reticuleaoesmall")
	self.ind:AddComponent("colourtweener")
	self.ind.AnimState:SetMultColour(unpack({0,0,0,0}))
	self.ind.components.colourtweener:StartTween(WEBCOLOURS.SPRINGGREEN, .1)
end

function LandIndicator:RemoveIndicator()
	if self.ind then
		self.ind.components.colourtweener:StartTween({0,0,0,0}, .3, function()
			self.ind:Remove()
			self.ind = nil
		end)
	end
end

function LandIndicator:OnUpdate(dt)
	local x,y,z = self.inst.Transform:GetWorldPosition()
	self.ind.Transform:SetPosition(x, 0, z)
	
	if not self.inst:IsOnValidGround() then
		self.ind.components.colourtweener:StartTween(WEBCOLOURS.FIREBRICK, .1)
	else
		self.ind.components.colourtweener:StartTween(WEBCOLOURS.SPRINGGREEN, .1)
	end
end

return LandIndicator