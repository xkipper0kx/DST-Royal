local FlightTracker = Class(function(self, inst)
    self.inst = inst
    self.dist_to_ground = net_float(inst.GUID, "flight_tracker._dist_to_ground", "distdirty")
	
	if not TheNet:IsDedicated() then
		inst:ListenForEvent("distdirty", function() self:OnDistChanged() end)
	end
end)

function FlightTracker:SetDistToGround(val)
	self.dist_to_ground:set(val)
end

function FlightTracker:IsFlying()
	return self.dist_to_ground:value() > 0
end

function FlightTracker:OnDistChanged()
	if not TheCamera then return end
	
	if self:IsFlying() then
		TheCamera.mindistpitch = math.clamp(90 - (60 - self.dist_to_ground:value() * 1.5), 30, 90)
	else
		TheCamera:SetDefault()
	end
end

return FlightTracker