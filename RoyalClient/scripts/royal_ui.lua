local TEMPLATES = require "widgets/redux/templates"
local Grid = require "widgets/grid"

local function AdjustPlayerGrid(self)
	local screen_width = 812 -- This was found through testing
	local widget_width = 125
	local widget_height = 250
	local offset_width = 125
	local offset_height = 30
	local col = 3
	local row = 2
	local scalar = 2/row
	while col*row < #self.player_listing do
		if (col * (widget_width + offset_width) - offset_width) * scalar > screen_width then
			row = row + 1
			scalar = 2 / row
		else
			col = col + 1
		end
	end
	local scalars = {}
	
	-- Scale each widget based on number of players
	for i, widget in pairs(self.player_listing) do
		widget:SetScale(scalar)
	end
	-- Clear and Update grid based on amount of players
	local old_grid = self.list_root
	self.list_root = self.proot:AddChild(Grid())
	self.list_root:FillGrid(col, (widget_width + offset_width) * scalar, (widget_height + offset_height) * scalar, self.player_listing)
	self.list_root:SetPosition(-(widget_width + offset_width) * scalar * col / 2 + offset_width * scalar, (#self.player_listing > 3 and ((widget_height + offset_height)*scalar*(row - 1)/2) or 0) + 20)
	old_grid:Kill()
	self:RefreshPlayersReady()
	--Так же мы убираем кнопку для старта, потому что у нас всегда убдет отбратный отсчет
	local _RefreshPlayersReady = self.RefreshPlayersReady
	function self:RefreshPlayersReady()
		_RefreshPlayersReady(self)
		
		self.playerready_checkbox:Disable()
		self.playerready_checkbox:Hide()
		
		for i, widget in ipairs(self.player_listing) do
			widget._playerreadytext:Hide()
		end
	end
	
	self.inst:ListenForEvent("_lobbyplayerspawndelay", function(world, data)
		if data and data.active then
			self.spawn_countdown_active = true
			self:RefreshPlayersReady()

            --subtract one so we hang on 0 for a second
            local str = subfmt(STRINGS.UI.LOBBY_WAITING_FOR_PLAYERS_SCREEN.SPAWN_DELAY, { time = str_seconds(math.max(0, data.time - 1)) })
            if str ~= spawndelaytext:GetString() or not spawndelaytext.shown then
                spawndelaytext:SetString(str)
                spawndelaytext:Show()
				
				if data.time <= 30 then
					TheFrontEnd:GetSound():PlaySound("dontstarve/HUD/WorldDeathTick")
				end
            end
		end
	end, TheWorld)
end

return {
	["widgets/waitingforplayers"] =  function(...) end,
}
