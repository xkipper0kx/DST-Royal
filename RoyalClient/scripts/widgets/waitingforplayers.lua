local Grid = require "widgets/grid"
local PlayerAvatarPortrait = require "widgets/redux/playeravatarportrait"
local Widget = require "widgets/widget"
local ImageButton = require "widgets/imagebutton"
local UserCommands = require "usercommands"
local Text = require "widgets/text"
local PopupDialogScreen = require "screens/redux/popupdialog"

local TEMPLATES = require "widgets/redux/templates"

local item_swap_overrides =
{
	lavaarena_lucy = "swap_lucy_axe",
	book_fossil = "", -- books are not heald in hands
	lavaarena_armorlight = "armor_light",
    lavaarena_armorlightspeed = "armor_lightspeed",
    lavaarena_armormedium = "armor_medium",
}

--------------------------------------------------------------------------
--  A grid of player puppets for the lobby screen "player ready, waiting for other players" screen
--
local WaitingForPlayers = Class(Widget, function(self, owner, max_players)
    self.owner = owner
    Widget._ctor(self, "WaitingForPlayers")

    self.players = self:GetPlayerTable()

    self.proot = self:AddChild(Widget("ROOT"))

	self.player_listing = {}
	for i = 1, max_players do
		local portrait = PlayerAvatarPortrait()
		portrait.frame:SetScale(.43)

        table.insert(self.player_listing, portrait)
	end
	
	local screen_width = 812 -- This was found through testing
	local widget_width = 125
	local widget_height = 250
	local offset_width = 125
	local offset_height = 30
	local col = 3
	local row = 2
	local scalar = 2/row
	while col*row < #self.player_listing do
		if (col * (widget_width + offset_width) - offset_width) * scalar > screen_width then
			row = row + 1
			scalar = 2 / row
		else
			col = col + 1
		end
	end
	local scalars = {}
	
	-- Scale each widget based on number of players
	for i, widget in pairs(self.player_listing) do
		widget:SetScale(scalar)
	end
	
	-- Clear and Update grid based on amount of players
	self.list_root = self.proot:AddChild(Grid())
	self.list_root:FillGrid(col, (widget_width + offset_width) * scalar, (widget_height + offset_height) * scalar, self.player_listing)
	self.list_root:SetPosition(-(widget_width + offset_width) * scalar * col / 2 + offset_width * scalar, (#self.player_listing > 3 and ((widget_height + offset_height)*scalar*(row - 1)/2) or 0) + 20)
end)

function WaitingForPlayers:IsServerFull()
	return #self.players == TheNet:GetServerMaxPlayers()
end

function WaitingForPlayers:GetPlayerTable()
    local ClientObjs = TheNet:GetClientTable()
    if ClientObjs == nil then
        return {}
    elseif TheNet:GetServerIsClientHosted() then
        return ClientObjs
    end

    --remove dedicate host from player list
    for i, v in ipairs(ClientObjs) do
        if v.performance ~= nil then
            table.remove(ClientObjs, i)
            break
        end
    end
    return ClientObjs 
end

local function UpdatePlayerListing(widget, data)
    local empty = data == nil or next(data) == nil
	
    widget.userid = not empty and data.userid or nil
    widget.performance = not empty and data.performance or nil

    if empty then
        widget:SetEmpty()
    else
        local prefab = data.lobbycharacter or data.prefab or ""
        widget:UpdatePlayerListing(data.name, data.colour, prefab, GetSkinsDataFromClientTableData(data))
    end
end

function WaitingForPlayers:Refresh(force)
    local prev_num_players = self.players ~= nil and #self.players or 0
    self.players = self:GetPlayerTable()

    for i, widget in ipairs(self.player_listing) do
        local player = self.players[i]
        if force or player == nil or
            player.userid ~= widget.userid or
            player.lobbycharacter ~= widget.lobbycharacter or
            (player.performance ~= nil) ~= (widget.performance ~= nil)
            then
            UpdatePlayerListing(widget, player)
        end
    end
end

function WaitingForPlayers:OnControl(control, down)
	if Widget.OnControl(self, control, down) then return true end
end

return WaitingForPlayers
