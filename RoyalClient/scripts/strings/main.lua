local s = STRINGS
local nm = STRINGS.NAMES
local desc = STRINGS.CHARACTERS.GENERIC.DESCRIBE
local gen = STRINGS.CHARACTERS.GENERIC.DESCRIBE
local rec = STRINGS.RECIPE_DESC 

s.EMOJI = {
	BEEFALO = "󰀁",
	COOKPOT = "󰀄",
	FARM = "󰀇",
	FIRE = "󰀈",
	GHOST = "󰀉",
	EYE = "󰀅",
	GRAVE = "󰀊",
	HAM_BAT = "󰀋",
	HAMMER = "󰀌",
	HEART = "󰀍",
	STOMACH = "󰀎",
	BULB = "󰀏",
	MAGIC = "󰀀",
	POOP = "󰀑",
	RED_GEM = "󰀒",
	BRAIN = "󰀓",
	SCIENCE = "󰀔",
	TEETH = "󰀆",
	SKULL = "󰀕",
	SILK = "󰀗",
}
--------------------------------

nm.DROWNING = "drowning"