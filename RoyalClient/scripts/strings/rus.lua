--Нативная поддержка русского языка
local s = STRINGS
local nm = s.NAMES
local desc = s.CHARACTERS.GENERIC.DESCRIBE
local gen = s.CHARACTERS.GENERIC.DESCRIBE
local rec = s.RECIPE_DESC 

local rus = mods.RussianLanguagePack
local RegisterRussianName = rus and rus.RegisterRussianName

if RegisterRussianName then
	--key,val,gender,walkto,defaultaction,capitalized,killaction
	RegisterRussianName("DROWNING", "Утопление", 4, "Утоплению", 1, false, "утоплением")
end

