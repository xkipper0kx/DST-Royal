local STATES_TO_ADD = {
	State{
        name = "fly_idle",
        tags = { "idle", "canrotate", "notalking" },

        onenter = function(inst)
			print "client: fly_idle"
            inst.entity:SetIsPredictingMovement(false)
			
			inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()
			
            inst.AnimState:PlayAnimation("flying_loop", true)
        end,

        onexit = function(inst)
            inst.entity:SetIsPredictingMovement(true)
        end,
    },

	State{
        name = "fly_start",
        tags = { "moving", "running", "canrotate", "notalking"  },

        onenter = function(inst)
			print "client: fly_start"
            inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("flying_loop")
			print(tostring(inst.AnimState:GetCurrentAnimationLength()))
            inst.sg.mem.footsteps = 0
			
			--inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,
		--[[
        ontimeout = function(inst)
			inst.sg:GoToState("fly")
		end,]]
		
		events =
        {
            EventHandler("animover", function(inst)
				print "client: animover"
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("fly")
                end
            end),
        },
    },

    State{
        name = "fly",
        tags = { "moving", "running", "canrotate", "notalking" },

        onenter = function(inst)
			print "client: fly"
            inst.components.locomotor:RunForward()

			inst.AnimState:PlayAnimation("flying_loop", true)
            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("fly")
        end,
    },

    State{
        name = "fly_stop",
        tags = { "canrotate", "idle", "notalking"},

        onenter = function(inst, stop)
			print "client: fly_stop"
            inst.components.locomotor:Stop()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
}

--Добавляем
for i, state in ipairs(STATES_TO_ADD) do
	AddStategraphState("wilson_client", state)
end

--Перехватываем ивенты, отвечающие за передвижение, и вставляем своё
AddStategraphPostInit("wilson_client", function(inst)
    local _locomotefn = inst.events.locomote.fn
	
	inst.events.locomote.fn = function(inst, data, ...)
		if inst.sg:HasStateTag("busy") or inst:HasTag("busy") then
			return
		end
		
		local is_moving = inst.sg:HasStateTag("moving")
		local should_move = inst.components.locomotor:WantsToMoveForward()
		if inst.replica.flight_tracker and inst.replica.flight_tracker:IsFlying() then
			if is_moving and not should_move then
				inst.sg:GoToState("fly_stop")
			elseif not is_moving and should_move then
				inst.sg:GoToState("fly")
			end
			
			return
		end

		return _locomotefn(inst, data, ...)
	end
	
	local _idleonenter = inst.states.idle.onenter
	inst.states.idle.onenter = function(inst, ...)
		if inst.replica.flight_tracker and inst.replica.flight_tracker:IsFlying() then
			inst.sg:GoToState("fly_idle")
			return
		end
		
		_idleonenter(inst, ...)
	end
end)
