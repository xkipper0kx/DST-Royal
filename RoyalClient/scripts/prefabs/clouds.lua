return Prefab(
	"clouds",
	function()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()

		inst.AnimState:SetBank("clouds_ol")
		inst.AnimState:SetBuild("clouds_ol")
		inst.AnimState:PlayAnimation("idle", true)
		inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
		inst.AnimState:SetLayer(LAYER_BACKGROUND)
		inst.AnimState:SetSortOrder(1)
		
		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
		
		

		return inst
	end
)
