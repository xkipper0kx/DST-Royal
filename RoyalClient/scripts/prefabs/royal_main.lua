local function CreatPrefab(data)
	local function fn()
		local inst = CreateEntity()

		if data.engine_components then
			for component, _ in pairs(data.engine_components) do
				inst.entity["Add"..component](inst.entity)
			end
		end

		if data.tags then
			for i, tag in ipairs(data.tags) do
				inst:AddTag(tag)
			end
		end
		
		if data.fn then
			data.fn(inst)
		end

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		local server_fn = TheServerData:GetServerData("prefabs")
		if server_fn and server_fn[data.name] then
			server_fn[data.name](inst)
		end

		return inst
	end

	return Prefab(data.name, fn, data.assets)
end

local prefs = {}
local pref_dat = require("royal_prefabs")

for _, data in ipairs(pref_dat) do
    table.insert(prefs, CreatPrefab(data))
end

return unpack(prefs)
