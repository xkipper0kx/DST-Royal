_G = GLOBAL
_version = "1.20"

print(string.format("%s: Initializing Lib. Ver: %s", modinfo.name, _version)) --Show current version of lib

--This is mini module for not executing library twice.

mods = _G.rawget(_G,"mods")
if not mods then
	mods = {}
	_G.rawset(_G,"mods", mods)
end

if mods.lib and mods.lib.version >= _version then --Only if needed.
	print(modinfo.name..": Using existing lib... "..mods.lib.version)
else
	print(modinfo.name..": Initializing the lib... ".._version,mods.lib and "(old: "..mods.lib.version..")")
	modimport "scripts/libs/lib_fn.lua"
	mods.lib.version = _version
end
	
mods.lib.ExportLib(env)
modimport "scripts/libs/debug.lua"