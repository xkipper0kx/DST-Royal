--By Star
local pl=AllPlayers

GetGlobal("pl",pl)
GetGlobal("me","me")
GetGlobal("all","all")

----------------------------------------------------------------
local ANNOUNCE_CATEGORIES = {leave_game=1,join_game=1,}
local c_announce = function(s,category)
	TheNet:Announce( s, nil, nil, category ) --Неправильная категория крашнет игру.
end
GetGlobal("a",c_announce)

SHORTCUTS = {
	--portal = 'multiplayer_portal', --И так найдет по части названия.
}

--Просто возвращает переменную, при этом выводит ее.
local function get_Show(var, txt)
	txt = txt or 'Found:'
	print(txt, tostring(var))
	return var
end

--Получает инстанс (игрока или объекта) по имени, номеру и т.д.
local function get(var, txt) --txt - что выводить в подсказке. По умолчанию "Found:"
	--[[ Порядок подбора:
	1) Полный никнейм.
	2) lower полный префаб.
	3) Часть ника.
	4) lower часть префаба.
	5) any case часть ника.
	--]]
	if type(var) == "string" then --самая популярная тема
		--if SHORTCUTS[var] then --используется короткий синоним.
		--	var = SHORTCUTS[var] --заменяем на полный.
		--end
		local low = string.lower(var)
		--Ищем по ключевым словам.
		if low == "me" then
			return get_Show(_G.ThePlayer, txt)
		end
		--Сначала ищем по имени. 1) Полный никнейм.
		for k,v in ipairs(pl) do
			if var == v.name then
				return get_Show(v, txt)
			end
		end
		--Далее ищем по префабу. Это необязательный шаг.
		for k,v in ipairs(pl) do
			if low == v.prefab then
				return get_Show(v, txt)
			end
		end
		--Получаем координаты игрока.
		local x,y,z = 0,0,0
		local player = _G.ConsoleCommandPlayer()
		if player then
			x,y,z = player.Transform:GetWorldPosition()
		end
		--Иначе... даем шанс. А вдруг есть такой предмет в мире? Ищем по префабу
		local ents = TheSim:FindEntities(x,y,z,2500,nil,{"FX", "NOCLICK", "DECOR", "INLIMBO"})
		for i,v in ipairs(ents) do
			if v.prefab == low and v.Transform then
				return get_Show(v, txt)
			end
		end
		--Пробуем как часть ника.
		for k,v in ipairs(pl) do
			if v.name:find(var,1,true) then
				return get_Show(v, txt)
			end
		end
		--Продуем как lower часть префаба.
		for i,v in ipairs(ents) do
			if v.prefab:find(low,1,true) then
				return get_Show(v, txt)
			end
		end
		--Пробуем как any case часть ника.
		for k,v in ipairs(pl) do
			if v.name:lower():find(low,1,true) then
				return get_Show(v, txt)
			end
		end
	elseif type(var)=="number" then
		return get_Show(pl[var], txt)
	elseif type(var)=="table" then
		return var --глупо, но бывает
	elseif type(var)=="function" then
		for k,v in ipairs(ents) do
			if var(v) then --подходит / не подходит
				return get_Show(v, txt)
			end
		end
	end
	--иначе кукиш, то есть nil
end
--Публичная версия также печатает никнейм в паблик
--[[GetGlobal("get",function(var)
	local res = get(var)
	if not res then
		print("NO PLAYER")
	else
		print("NICKNAME: "..tostring(res.name))
	end
	return res
end)--]]
_G.get = get --Выносим нагло, потому что это ключевая функция работы мода.

--То же (копипаст), но учитываются только живые.
local function get_alive(var, txt) --txt - что выводить в подсказке. По умолчанию "Found:"
	--[[ Порядок подбора:
	1) Полный никнейм.
	2) lower полный префаб.
	3) Часть ника.
	4) lower часть префаба.
	5) any case часть ника.
	--]]
	if type(var) == "string" then --самая популярная тема
		--if SHORTCUTS[var] then --используется короткий синоним.
		--	var = SHORTCUTS[var] --заменяем на полный.
		--end
		local low = string.lower(var)
		--Ищем по ключевым словам.
		if low == "me" then
			return get_Show(_G.ThePlayer, txt)
		end
		--Сначала ищем по имени. 1) Полный никнейм.
		for k,v in ipairs(pl) do
			if var == v.name and v.components.health and v.components.health.currenthealth > 0 then
				return get_Show(v, txt)
			end
		end
		--Далее ищем по префабу. Это необязательный шаг.
		for k,v in ipairs(pl) do
			if low == v.prefab and v.components.health and v.components.health.currenthealth > 0 then
				return get_Show(v, txt)
			end
		end
		--Получаем координаты игрока.
		local x,y,z = 0,0,0
		local player = _G.ConsoleCommandPlayer()
		if player then
			x,y,z = player.Transform:GetWorldPosition()
		end
		--Иначе... даем шанс. А вдруг есть такой предмет в мире? Ищем по префабу
		local ents = TheSim:FindEntities(x,y,z,2500,nil,{"FX", "NOCLICK", "DECOR", "INLIMBO"})
		for i,v in ipairs(ents) do
			if v.prefab == low and v.Transform and v.components.health and v.components.health.currenthealth > 0 then
				return get_Show(v, txt)
			end
		end
		--Пробуем как часть ника.
		for k,v in ipairs(pl) do
			if v.name:find(var,1,true) and v.components.health and v.components.health.currenthealth > 0 then
				return get_Show(v, txt)
			end
		end
		--Продуем как lower часть префаба.
		for i,v in ipairs(ents) do
			if v.prefab:find(low,1,true) and v.components.health and v.components.health.currenthealth > 0 then
				return get_Show(v, txt)
			end
		end
		--Пробуем как any case часть ника.
		for k,v in ipairs(pl) do
			if v.name:lower():find(low,1,true) and v.components.health and v.components.health.currenthealth > 0 then
				return get_Show(v, txt)
			end
		end
	elseif type(var)=="number" then
		return get_Show(pl[var], txt)
	elseif type(var)=="table" then
		return var --глупо, но бывает
	elseif type(var)=="function" then
		for k,v in ipairs(ents) do
			if var(v) and v.components.health and v.components.health.currenthealth > 0 then --подходит / не подходит
				return get_Show(v, txt)
			end
		end
	end
	--иначе кукиш, то есть nil
end

local p=function(...)
	local arg = {...}
	if #arg == 0 then
		_G.print('nil')
	else
		local s = tostring(arg[1])
		for i=2,#arg do
			s = s..", "..tostring(arg[i])
		end
		_G.print(s)
	end
end
GetGlobal("p",p)

local const_timers = {1,2,3,4,10,15,30,60,120,180,240,300,600,900,1200,1800,}
local function shutdown(s,time_out,mess)
	if not time_out then
		if type(s)=="number" then
			time_out = s
		else
			time_out = 60
		end
	end
	time_out = time_out + 1
	if type(s)=="string" then
		c_announce(s,"leave_game")
	end
	local w = _G.TheWorld
	if w then
		local shut_func = function(inst,sec)
			if sec>61 then
				c_announce("The server will be "..mess.." in "..tostring(rnd2(sec/60)).." minutss.","leave_game")
			else
				c_announce("The server will be "..mess.." in "..tostring(sec).." second"..(sec==1 and "" or "s")..".","leave_game")
			end
			if sec<=60 and sec>=15 then
				c_announce("Find a safe place.","leave_game")
			end
		end
		w:DoTaskInTime(time_out,function()
			if mess == "rebooted" then
				_G.c_reset(true)
			else
				_G.c_shutdown(true)
			end
		end)
		for _,v in ipairs(const_timers) do
			local tm = time_out - v
			if tm > 0.5 then --уведомление попадает в обратный отсчет
				w:DoTaskInTime(tm,shut_func,v) --передаем текущее значение как параметр для вывода
			end
		end
	end
end

GetGlobal("shutdown",function(s,time_out)
	shutdown(s,time_out,"shutdown")
end)

GetGlobal("reboot",function(s,time_out)
	shutdown(s,time_out,"rebooted")
	_G.TheWorld:DoTaskInTime(time_out-3,function()--за 3 секуны до ресета сохраняем
		_G.c_save()
	end)
end)


--Добавляем немного инфы по действиям.
local function AddLittleActInfo(name,fn)
	return function(act,...)
		print("ACTION - "..tostring(name)..", "..tostring(act.doer)..", "..tostring(act.target))
		if fn then
			return fn(act,...)
		end
	end
end
local show_act_activated = false
GetGlobal("show_act",function()
	if show_act_activated then
		return
	else
		show_act_activated = true
	end
	for k,v in pairs(_G.ACTIONS) do
		v.fn = AddLittleActInfo(k,v.fn) --Добавляем инфо только по требованию!
	end
end)

--Меняем пространство имен мода
--mod - название мода или steam id (работает только имя папки)
local saved_env
GetGlobal("env",function(mod)
	if saved_env ~= nil and mod == nil then
		return saved_env
	end
	if type(mod)=="number" then
		mod = "workshop-"..mod
	else
		mod = tostring(mod)
	end
	local res = _G.ModManager:GetMod(mod)
	if res == nil then
		print('Mod "'..mod..'" does not exist.')
		return
	end
	saved_env = res.env
	return saved_env
end)

local saved_modinfo
GetGlobal("modinfo",function(mod)
	if saved_modinfo and not mod then
		return saved_modinfo
	end
	if type(mod)=="number" then
		mod = "workshop-"..mod
	else
		mod = tostring(mod)
	end
	local res = _G.ModManager:GetMod(mod).modinfo
	saved_modinfo = res
	return res
end)

--Сбрасывает таблицу в файл.
--Показ таблицы отключен для экономии процессора и памяти.
local io=_G.io
local save_tables={}
local arr_deep_num = 900
--Нестандартный вызов: arr(obj,{filename})
local arr
arr=function(o,t,f)
	arr_deep_num = type(t)=="number" and t or arr_deep_num --это же количество уровней вложенности при первом вызове
	if arr_deep_num < 1 then --уровень себя исчерпал
		f:write(t.."[too deep]\n")
		return false
	end
	if not t or type(t)~="string" or not f then
		local filename = "log.txt"
		if type(t)=="table" and type(t[1])=="string" then
			filename = t[1]
		end
		t=""
		save_tables={}
		f=io.open(filename,"w")
		f:write("Формат: *ключ = значение\n"
			.."Перед ключом - звёздочка.\n"
			.."Если ключ не является строкой или числом, то он заключается в [type(ключ):tostring(ключ)]\n"
			.."Если значение является строкой, то показывается в кавычках.\n"
			.."Если значение не является строкой или числом, то в скобках указывается тип.\n"
			.."Если ключ не parent и значение - таблица, которая не встрачалась, то идёт рекурсия.\n"
		)
	end
	if save_tables[o] then
		--print(t.."[recursive]")
		f:write(t.."[recursive]\n")
		return false
	end
	arr_deep_num = arr_deep_num - 1
	save_tables[o]=1
	if type(o)=="table" then
		for k,v in pairs(o) do
			local kk=type(k)=="string" and "\""..k.."\"" or 
				(type(k)=="number" and k or 
				("["..type(k)..":"..tostring(k).."]"))
			--print(t..kk.." = "..tostring(v))
			f:write(t.."*"..kk
				..(type(v)~="string" and type(v)~="number" and " ("..type(v)..")" or "")
				.." = "
				..(type(v)=="string" and "\"" or "")
				..tostring(v)
				..(type(v)=="string" and "\"" or "")
				.."\n")
			if type(v)=="table" and k~="parent" and k~="inst" and k~="owner" then arr(v,t.."\t",f) end
		end
	else
		f:write("NOT_A_TABLE: "..tostring(o).."\n")
	end
	if(t=="") then
		f:close()
	end
	arr_deep_num = arr_deep_num + 1
end
GetGlobal("arr",arr)

--удаляет все пребафы из мира.
--Если префаб не указан, то удаляет вообще все

--option:
-- Если true, то игнорирует предметы в инвентаре. Если "hp", то удаляет только те, кто с макс хп.
--Если "player", то удаляет только те, которые далеко от игроков.
--Если это функция(inst), то удаляет только те, для кого она true
GetGlobal("del_all",function(prefab,option,force_valid)
	local e=TheSim:FindEntities(0,0,0,1500)
	--local res={}
	local j=0
	for i,v in ipairs(e) do
		if ((v.persists and (not v:HasTag("player")) and v:IsValid()) or force_valid)
			and (v.prefab == prefab or not prefab)
		then
			local must_remove = true
			if option then
				if option == true then
					must_remove = not v.inlimbo
				elseif option == "hp" then
					must_remove = (v.components.health and v.components.health.currenthealth == v.components.health.maxhealth)
				elseif option == "player" then
					for _,p in ipairs(pl) do
						if p:GetDistanceSqToInst(v) < 1225 then --35*35
							must_remove = false
							break
						end
					end
				elseif type(option)=="function" then
					must_remove = option(v)
				else
					p("ERROR: bad option")
					break
				end
			end
			--local s=v.prefab
			if must_remove then
				v:Remove()
				j=j+1
			end
			--res[j]=tostring(s).." = "..tostring(r)
		end
	end
	--arr(res)
	p("DELETED "..j.."/"..(#e).." PREFABS")
end)



--find objects in the world
local fe=function(prefab,range,coords)
	if not range then range=50 end
	--local p=_G.GetPlayer()
	local x,y,z = 0,0,0
	if coords then
		x,y,z=coords[1],coords[2],coords[3]
	else
		local p=_G.ThePlayer
		if p and p.Transform then
			x,y,z = p.Transform:GetWorldPosition()
		end
	end
	local ents = _G.TheSim:FindEntities(x,y,z, range) --ищем все объекты - 2 экрана
	local a={}
	local function cmp(a,b)
		return a.dist<b.dist
	end
	if type(prefab)=="string" then --single prefab
		if prefab=="any" then
			if #ents>=1 then
				return true
			else
				return false
			end
		else
			for i=1,#ents do
				if ents[i].prefab == prefab then
					local x1,y1,z1=ents[i].Transform:GetWorldPosition()
					table.insert(a,{inst=ents[i], dist=(x1-x)*(x1-x)+(y1-y)*(y1-y)+(z1-z)*(z1-z)})
				end
			end
		end
	else --few prefabs
		for i=1,#ents do
			local e=ents[i]
			for j=1,#prefab do
				if e.prefab == prefab[j] then
					local x1,y1,z1=e.Transform:GetWorldPosition()
					table.insert(a,{inst=e, dist=(x1-x)*(x1-x)+(y1-y)*(y1-y)+(z1-z)*(z1-z)})
					break
				end
			end
		end
	end
	--table.sort(a,cmp)
	--ищем первый объект, расстояние до которого больше 0 (но это самое маленькое расстояние)
	for i=1,#a do
		if a[i].dist>0 then
			return a[i].inst
		end
	end
	--возвращаем просто первый объект (дистанция 0)
	if #a>0 then
		return a[1].inst
	end
	--иначе пусто?
	--print("No such objects in range.")
	return false
end
GetGlobal("fe",fe)

--Анализ мира
io=_G.io
_G.ca_all = function(range)
	if not range then range=1500 end
	local f=io.open("world.txt","w")
	local a = {} --основная таблица счетчиков
	local n = {} --обратные ассоциации для сортировки
	local ents = TheSim:FindEntities(0,0,0,range)
	for i,v in ipairs(ents) do
		local pref=v.prefab
		if not pref then pref="No Prefab" end
		if not a[pref] then
			a[pref]={pers=0, nonpers=0, total=0, key=pref, awake=0, awake_down=0, tags={}}
			table.insert(n,pref)
		end
		a[pref].total=a[pref].total+1
		if (v.persists) then
			a[pref].pers = a[pref].pers+1
			if v.entity and v.entity:IsAwake() then
				a[pref].awake=a[pref].awake+1
			end
			if v.tags then
				for j,w in ipairs(v.tags) do
					local w=v.tags[j]
					if not a[pref].tags[w] then
						a[pref].tags[w]=1
					else
						a[pref].tags[w]=a[pref].tags[w]+1
					end
				end
			end
		else
			a[pref].nonpers = a[pref].nonpers+1
			if v.entity and v.entity:IsAwake() then
				a[pref].awake_down=a[pref].awake_down+1 --странные активные объекты, не находящиеся на карте
			end
		end
		
	end
	local function cmp(x,y)
		return a[x].total>a[y].total
	end
	table.sort(n,cmp)
	local sum=0
	local sum_awaken = 0
	for i,v in ipairs(n) do
		v=a[v]
		local s=v.key.."\t"..v.total
		local ss="("
		if v.awake>0 then
			ss=ss.."AWAKE="..v.awake..", "
			sum_awaken = sum_awaken + v.awake
		end
		if v.awake_down>0 then
			ss=ss.."awDown="..v.awake_down..", "
			sum_awaken = sum_awaken + v.awake_down
		end
		if v.nonpers>0 then
			ss=ss.."notPersists="..v.nonpers..", "
		end
		if #(v.tags)>0 then
			ss=ss.." { "
			for k,v in pairs(v.tags) do
				ss=ss..k.."="..v..", "
			end
			ss=ss..ss.."}"
		end
		if ss~="(" then
			s=s.."\t"..ss..")"
		end
		f:write(s.."\n")
		sum = sum + v.total
	end
	f:write("SUM = "..sum.."\n")
	f:write("AWAKEN = "..sum_awaken.."\n")
	f:close()
end


--Replace all substrings in a file
-- a - массив пар вхождений. Например PatchFile("scripts/components/container.lua",{{"what","repl"}, {"    ","\t"},} )
local function PatchFile(filename,a)
	--p("Filename: "..filename)
	local f=io.open(filename,"r")
	if not f then
		p("ERROR: file "..filename.." not found!")
		return
	end
	local content = f:read("*all")
	f:close()
	--Now we have content (a string).
	--[[local function literalize(str)
		return str:gsub("[%(%)%.%%%+%-%*%?%[%]%^%$]", function(c) return "%" .. c end)
	end--]]
	if a.stop_pattern and content:find(a.stop_pattern) then
		return --уже пропатчено
	end
	local sum = 0
	for i=1,#a do
		local cnt = content:find(a[i][1]) --,1,true)
		if cnt ~= nil then
			local v=a[i]
			--content = content:gsub(literalize(v[1]),v[2])
			content, cnt = content:gsub(v[1],v[2])
			sum = sum + cnt
		end
	end
	--p("SUM: "..tostring(sum))
	if sum > 0 then --rewrite the file
		f=io.open(filename,"w")
		if not f then
			p("ERROR: file "..filename.." is not writeable!")
			return
		end
		f:write(content)
		f:close(f)
	end
end
GetGlobal("PatchFile",PatchFile)

-------------------Server Side--------------------
if _G.TheNet and _G.TheNet:GetIsServer() then

	local function rain(num)
		_G.TheWorld:PushEvent("ms_deltamoisture",num) --_G.TheWorld.state.moisture + num)
	end
	GetGlobal("rain",rain)

	local function GiveAllRecipes(player)
		local player = player or _G.ThePlayer
		if player ~= nil then
			if player == 1 then
				for k,v in pairs(AllPlayers) do
					v.components.builder:GiveAllRecipes()
				end
			else
				if player.components.builder then
					player.components.builder:GiveAllRecipes()
				end
			end
		else
			_G.ThePlayer.components.builder:GiveAllRecipes()
		end
	end

	GetGlobal("rec",GiveAllRecipes)
end


-----------Инициализация мира------------
local is_forest
local function ForestInit(inst)
	if is_forest then
		return
	end
	is_forest=true
	GetGlobal("w",inst)
	GetGlobal("ww",inst.state)
end
AddPrefabPostInit("world",ForestInit)
AddPrefabPostInit("forest",ForestInit)


--чит карты
local REVEAL_SRC = { evergreen=1, grass=1, sapling=1, deciduoustree=1, evergreen_sparse=1, marsh_tree=1,
	flower=1, tentacle=1, rabbithole=1, 
	rock_flintless=1, marsh_bush=1, rock1=1, tumbleweed=1, fireflies=1, rock2=1, berrybush=1, cactus=1, reeds=1,
	carrot_planted=1, green_mushroom=1, molehill=1, pighouse=1, mermhouse=1,
}
GetGlobal("r",function(player)
	--_G.TheWorld.minimap.MiniMap:ShowArea(0,0,0,10000)
	--[[local owner = _G.ThePlayer
	if not (owner and owner.components and owner.components.maprevealable) then
		return print("Can't reveal")
	end
	local ents = TheSim:FindEntities(0,0,0,2500,nil,{"FX", "NOCLICK", "DECOR", "INLIMBO"})
	local cnt = 0
	for k,v in pairs(ents) do
		if REVEAL_SRC[v.prefab] then
			owner.components.maprevealable:AddRevealSource(v)
			cnt = cnt + 1
		end
	end
	print('Added to reveal:',cnt)--]]
	local owner = player and get(player) or _G.ThePlayer
	if not (owner and owner.player_classified and owner.player_classified.MapExplorer
		and owner.player_classified.MapExplorer.RevealArea)
	then
		return print("Bad player.")
	end
	for x=-1600,1600,35 do
		for y=-1600,1600,35 do
			owner.player_classified.MapExplorer:RevealArea(x,0,y)
		end
	end
end)

local function move(a,b)
	a = get(a,'To:')
	b = get(b,'From:')
	if not b then
		b = _G.ThePlayer --По умолчанию игрок (тот, кто вводит команду).
	end
	if not (a and b and a.Transform and b.Transform) then
		return print("Bad objects")
	end
	--print("Move "..tostring(a)..' to '..tostring(b))
	b.Transform:SetPosition(a.Transform:GetWorldPosition())
end
GetGlobal("move",move)
GetGlobal("moveto",move)
GetGlobal("teleport",move)

GetGlobal("pull",function(a)
	move(_G.ThePlayer,a)
end)

GetGlobal("speed",function(a,b)
	local player = _G.ThePlayer
	if b ~= nil then
		player = get(a)
		a = b
	end
	if not player then --player may be any object
		return print("Player not found.")
	end
	if a == false then
		a = 0.001
	elseif a == true then
		a = 1
	end
	if type(a) ~= 'number' then
		return print('Speed should be a number')
	end
	local loc = player.components and player.components.locomotor
	if not loc then
		return print("An object don't has locomotor component.")
	end
	loc.walkspeed = TUNING.WILSON_WALK_SPEED * a
	loc.runspeed = TUNING.WILSON_RUN_SPEED * a
	print('New speed:',tostring(math.floor(loc.runspeed * 100 + 0.5) .. '%'))
end)

GetGlobal("inv",function(a,b)
	local player = _G.ThePlayer
	if b ~= nil or type(a) == "string" then
		player = get(a)
		a = b
	end
	if not player then --player may be any object
		return print("Player not found.")
	end
	local h = player.components and player.components.health
	if not h then
		return print("An object don't has health component.")
	end
	local new = (a ~= false and a ~= 0) and true or false
	if player.guru_inv_task then
		player.guru_inv_task:Cancel()
		player.guru_inv_task = nil
	end
	if new then
		h:SetInvincible(true)
		player.guru_inv_task = player:DoPeriodicTask(0.5, function(inst)
			h:SetInvincible(true)
		end)
	else
		h:SetInvincible(false)
	end
end)

GetGlobal("kill",function(a,reason)
	if a == "all" then --Убиваем всех, кроме игроков, в радиусе 30
		local player = _G.ThePlayer or AllPlayers[1]
		if not player then return "Can't get current player." end
		local x,y,z = player.Transform:GetWorldPosition()--!
		local ents = TheSim:FindEntities(x,y,z,30,nil,{"FX", "NOCLICK", "DECOR", "INLIMBO"})
		local cnt = 0
		for k,v in pairs(ents) do
			if v.components and v.components.health and v.components.health.currenthealth > 0 then
				v.components.health:DoDelta(-999000, nil, reason and tostring(reason), nil, nil, true)
				cnt = cnt + 1
			end
		end
		if cnt > 0 then
			return print("Killed:",cnt)
		end
		return
	end
	a = get_alive(a)
	if not a then
		return print("Object not found.")
	end
	local h = a.components and a.components.health
	if not h then
		return print("An object don't has health component.")
	end
	h:DoDelta(-999000, nil, reason and tostring(reason), nil, nil, true)
end)


GetGlobal("circle",function(prefab,a,b,c) --(prefab,inst,radius,count) or (prefab,radius,count)
	if type(prefab) == "table" and prefab.prefab then
		prefab = prefab.prefab
	end
	if type(prefab) ~= "string" or not _G.Prefabs[prefab] then
		return print('You should specify the prefab name.')
	end
	--now prefab is spicified.
	local player = _G.ThePlayer
	if type(a) == "string" then
		player = get(a)
		a = b --shift other params
		b = c
	end
	if not (player and player.Transform) then --player may be any object
		return print("Player not found.")
	end
	---
	if type(b) ~= 'number' then
		b = 10
		print("Default number = 10.")
	end
	if type(a) ~= "number" then
		a = 10
		print("Default radius = 10")
	end
	--Draw circle:
	local radius = a
	local n = b
	local x0, y0, z0 = player.Transform:GetWorldPosition()
	local theta = math.random() * 6.28 --начальный угол
	for i=1,n do
		local alpha = i/n*6.28 + theta
		local x,z = x0+math.cos(alpha)*radius, z0+math.sin(alpha)*radius
		if _G.TheWorld.Map:GetTileAtPoint( x, 0, z ) == 1 then --_G.GROUND.IMPASSABLE
			print("Can't spawn "..tostring(prefab).." because of water tile.")
		else
			local ents = TheSim:FindEntities(x,0,z,1)
			if #ents > 0 then
				print("Can't spawn "..tostring(prefab).." because of:")
				for i,v in ipairs(ents) do
					print("\t"..tostring(v.prefab))
				end
			else
				local q = SpawnPrefab(prefab)
				if q then
					q.Transform:SetPosition(x, 0, z)
				else
					print("Can't spawn "..tostring(prefab))
				end
			end
		end
	end	
end)

local fe_nearest_forbid_prefabs = {edgefog=1,snow=1,rain=1,hail=1,}
--local fe_nearest_forbid_tags = {"player"}
local function fe_nearest()
	local x,y,z = _G.ThePlayer.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x,y,z,20) --ALL
	local MAX = 15
	local shown = 1
	print("----------------------------------------------")
	for i,v in ipairs(ents) do
		if shown > MAX then
			break
		end
		local dist = _G.ThePlayer:GetDistanceSqToInst(v)
		if not (v.inlimbo or fe_nearest_forbid_prefabs[v.prefab] or v:HasTag("player")) and dist ~= 0 then
			print(v.prefab,math.sqrt(dist))
			shown = shown + 1
		end
	end
end
GetGlobal("fe_nearest",fe_nearest)

local HELP_COMMANDS = {
	help = '',
	fe = 'Short of "FindEntities".\nSyntax: fe(prefab,range)',
}
GetGlobal("help",function(command)
	print('----- HELP INFO -----')
	if command and command ~= 'help' then
		if not HELP_COMMANDS[command] then
			print('Unknown command:',tostring(command))
		else
			print(HELP_COMMANDS[command])
		end
	else
		--print('This function will help you to see syntax of other commands.')
		print("Help is under construction... :(")
		print('Syntax: help [command]')
		print('Example: help "fe"')
		print('Commands: help, fe')
	end
end)

GetGlobal("scale",function(num)
	num = num or 1
	TheSim:SetTimeScale(num)
end)
