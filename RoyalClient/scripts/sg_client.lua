local states_to_add = {
	State
    {
        name = "fly_idle",
        tags = { "idle", "canrotate", "notalking" },

        onenter = function(inst, pushanim)
            inst.entity:SetIsPredictingMovement(false)
            inst.components.locomotor:Stop()
            inst.components.locomotor:Clear()

            if pushanim == "cancel" then
                return
            elseif inst:HasTag("nopredict") or inst:HasTag("pausepredict") then
                inst:ClearBufferedAction()
                return
            elseif pushanim == "noanim" then
                inst.sg:SetTimeout(TIMEOUT)
                return
            end

			if not inst.AnimState:IsCurrentAnimation("flying_loop") or inst.AnimState:AnimDone() then
				if pushanim then
					inst.AnimState:PushAnimation("flying_loop", true)
				else
					inst.AnimState:PlayAnimation("flying_loop", true)
				end
            end
        end,

        ontimeout = function(inst)
            if inst.bufferedaction ~= nil and inst.bufferedaction.ispreviewing then
                inst:ClearBufferedAction()
            end
        end,

        onexit = function(inst)
            inst.entity:SetIsPredictingMovement(true)
        end,
	},

	State
    {
        name = "fly_start",
        tags = { "moving", "running", "canrotate", "notalking" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
			
            inst.AnimState:PlayAnimation("flying_loop")
            inst.sg.mem.footsteps = 0
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,
		
        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("fly")
                end
            end),
        },
    },

    State
    {
        name = "fly",
        tags = { "moving", "running", "canrotate", "notalking" },

        onenter = function(inst)
            inst.components.locomotor:RunForward()
			
            if not inst.AnimState:IsCurrentAnimation("flying_loop") or inst.AnimState:AnimDone() then
                inst.AnimState:PlayAnimation("flying_loop", true)
            end

            inst.sg:SetTimeout(inst.AnimState:GetCurrentAnimationLength())
        end,

        onupdate = function(inst)
            inst.components.locomotor:RunForward()
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("fly")
        end,
    },

    State
    {
        name = "fly_stop",
        tags = { "canrotate", "idle", "notalking" },

        onenter = function(inst)
            inst.components.locomotor:Stop()
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },
}

for _, state in ipairs(states_to_add) do
	AddStategraphState("wilson_client", state)
end

AddStategraphPostInit("wilson_client", function(self)
	self.events.locomote.fn = function(inst, data)
		if inst.sg:HasStateTag("busy") or inst:HasTag("busy") then
            return
        end
		
        local is_moving = inst.sg:HasStateTag("moving")
        local should_move = inst.components.locomotor:WantsToMoveForward()
		local is_flying = inst.replica.flight_tracker and inst.replica.flight_tracker:IsFlying()
		
        if inst:HasTag("sleeping") then
            if should_move and not inst.sg:HasStateTag("waking") then
                inst.sg:GoToState("wakeup")
            end
        elseif not inst.entity:CanPredictMovement() then
            if not inst.sg:HasStateTag("idle") then
                inst.sg:GoToState("idle")
            end
        elseif is_moving and not should_move then
            inst.sg:GoToState((is_flying and "fly" or "run").."_stop")
        elseif not is_moving and should_move then
            inst.sg:GoToState((is_flying and "fly" or "run").."_start")
        end
	end
	
	local _idle = self.states.idle.onenter
	self.states.idle.onenter = function(inst, pushanim)
		if inst.components.flight_tracker and inst.components.flight_tracker:IsFlying() then
			inst.sg:GoToState("fly_idle", pushanim)
		else
			_idle(inst, pushanim)
		end
	end
end)

for _, state in ipairs(states_to_add) do
	AddStategraphState("wilson", state)
end

