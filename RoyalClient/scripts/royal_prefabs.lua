return {
	{
		name = "placeholder",
		
		engine_components = {
			Transform = true,
			Network = true,
			AnimState = true,
			SoundEmitter = false,
			MiniMapEntity = false,
			DynamicShadow = false,
			Follower = false,
			Light = false,
			LightWatcher = false
		},
		
		assets = {
			Asset("ANIM", "anim/player_basic.zip"),
		},
		
		tags = {
			"lightningtarget",
		},
		
		fn = function(inst)
			inst.AnimState:SetBank("wilson")
			inst.AnimState:SetBuild("wilson")
			inst.AnimState:PlayAnimation("death")
		end,
	},
}
