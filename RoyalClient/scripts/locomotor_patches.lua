local function GetFlightSpeed(inst)
	local is_flying = inst.components.flight_tracker and inst.components.flight_tracker:IsFlying()
	
	return is_flying and TUNING.PLAYER_FALL_SPPED or 0
end

local function WalkForward(self, direct)
    self.isrunning = false
    if direct then self.wantstomoveforward = true end
    self.inst.Physics:SetMotorVel(self:GetWalkSpeed(),GetFlightSpeed(self.inst),0)
    self:StartUpdatingInternal()
end

local function RunForward(self, direct)
    self.isrunning = true
    if direct then self.wantstomoveforward = true end
    self.inst.Physics:SetMotorVel(self:GetRunSpeed(),GetFlightSpeed(self.inst),0)
    self:StartUpdatingInternal()
end

return {
	--OnUpdate = OnUpdate,
	WalkForward = WalkForward,
	RunForward = RunForward
}
