return {
	Assets = {
		Asset("ANIM", "anim/player_flying.zip"),
		Asset("ANIM", "anim/player_boat_death.zip"),
	},
	
	Prefabs = {
		"royal_fx",
		"royal_main",
		"clouds",
	}
}