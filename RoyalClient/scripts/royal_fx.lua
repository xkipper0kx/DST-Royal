local function FinalOffset1(inst)
    inst.AnimState:SetFinalOffset(1)
end

local function FinalOffset2(inst)
    inst.AnimState:SetFinalOffset(2)
end

return {
    {
	    name = "boat_death",
	    bank = "boatdeathshadow",
	    build = "boat_death_shadows",
	    anim = "boat_death",
	    tintalpha = 0.5,
    },
}
