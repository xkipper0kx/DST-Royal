local ServerData = {
	version = 0.20,
	Cache = {} -- Если файл уже подгружен, то можно его не подгружать
}

function ServerData:GetServerData(path)
	if not TheNet:GetIsServer() or not mods.royal_server then
		print(
			string.format(
				"[Server Data]: Error! Tried to load data on client (%s, %s) from\n%s",
				tostring(TheNet:GetIsServer()),
				tostring(mods.royal_server),
				CalledFrom()
			)
		)
		
		return
	end
	
	local fullpath = "server_data/"..path
	print("[Server Data]: About to load "..fullpath..". Request from \n"..CalledFrom())
	
    if ServerData.Cache[fullpath] == nil then
		local res = softresolvefilepath("scripts/"..fullpath..".lua")
		
		if res ~= nil then
			ServerData.Cache[fullpath] = require(fullpath)
		else
			print("[Server Data]: ERROR!\nFrom: "..CalledFrom())
			return nil
		end
    end
	
	print("[Server Data]: Loaded "..fullpath)
	
    return ServerData.Cache[fullpath]
end

function ServerData:ClearCache()
	ServerData.Cache = nil
	ServerData.Cache = {}
	print("[Server Data]: ServerData.Cache cleared!")
end
	
function ServerData:Debug() -- TheServerData:Debug()
	print("[Server Data]: ServerData.Cache contains:\n{")
	
	for k,v in pairs(ServerData.Cache) do
		print("\t"..tostring(k)..",")
	end
	
	print("}")
end
	
return ServerData

